const oracledb = require('oracledb');
var SimpleOracleDB = require('simple-oracledb');
SimpleOracleDB.extend(oracledb);
let _db;

const Dbconnect_auroradb = () => {
    let dbAttr;

    if (process.env.CONNECTION === "LOCAL") {
        dbAttr = {
            user: "appnodejs",
            password: "Oracle_4U",
            connectString: `${process.env.ORACLE_HOSTNAME}:1521/APPPDB`
        }
    } else {
        dbAttr = {
            user: "CONVERGED",
            password: "Ora_DB4U",
            connectString: `${process.env.ORACLE_HOSTNAME}:1521/AWS_AURORA_PDB`
        }
    }
    return new Promise((resolve, reject) => {
        oracledb.getConnection(dbAttr)
            .then(connection => {
                _db = connection;
                resolve('Connected')
            })
            .catch((err) => {
                console.log(err)
            })
    })

}

const getDb_auroradb = () => {
    if (!_db) {
        // return _db;
        Dbconnect_auroradb().then(result => {
            return _db;
        })
    }
    if (_db) {
        return _db;
    }
}

module.exports = {
    Dbconnect_auroradb: Dbconnect_auroradb,
    getDb_auroradb: getDb_auroradb,
};
