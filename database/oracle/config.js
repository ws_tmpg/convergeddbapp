const oracledb = require('oracledb');
var SimpleOracleDB = require('simple-oracledb');
//oracledb.fetchAsBuffer = [oracledb.BLOB]
SimpleOracleDB.extend(oracledb);

let _db;

const Dbconnect_docdb = () => {
    let dbAttr;

    if (process.env.CONNECTION === "LOCAL") {
        dbAttr = {
            user: "appnodejs",
            password: "Oracle_4U",
            connectString: `${process.env.ORACLE_HOSTNAME}:1521/APPPDB`
        }
    } else {
        dbAttr = {
            user: "CONVERGED",
            password: "Ora_DB4U",
            connectString: `${process.env.ORACLE_HOSTNAME}:1521/AWS_DOCUMENTDB_PDB`
        }
    }
    return new Promise((resolve, reject) => {
        oracledb.getConnection(dbAttr)
            .then(connection => {
                _db = connection;
                resolve('Connected')
            })
            .catch((err) => {
                console.log(err)
            })
    })

}

const getDb_docdb = () => {
    if (!_db) {
        // return _db;
        Dbconnect_docdb().then(result => {
            return _db;
        })
    }
    if (_db) {
        return _db;
    }
}

module.exports = {
    Dbconnect_docdb: Dbconnect_docdb,
    getDb_docdb: getDb_docdb,
};
