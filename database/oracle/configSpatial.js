const oracledb = require('oracledb');
var SimpleOracleDB = require('simple-oracledb');
SimpleOracleDB.extend(oracledb);
let _db;

const Dbconnect2 = () => {
	let dbAttr;

	if (process.env.CONNECTION === "LOCAL") {
		dbAttr = {
			user: "appnodejs",
			password: "Oracle_4U",
			connectString: `${process.env.ORACLE_HOSTNAME}:1521/APPPDB`
		}
	} else {
		dbAttr = {
			user: "CONVERGED",
			password: "Ora_DB4U",
			connectString: `${process.env.ORACLE_HOSTNAME}:1521/APPPDB`
		}
	}
	return new Promise((resolve, reject) => {
		oracledb.getConnection(dbAttr)
			.then(connection => {
				_db = connection;
				resolve('Spatial DB Connected')
			})
			.catch((err) => {
				console.log(err)
			})
	})

}

const getDb2 = () => {
	if (!_db) {
		// return _db;
		Dbconnect2().then(result => {
			return _db;
		})
	}
	if (_db) {
		return _db;
	}
}

module.exports = {
	Dbconnect2: Dbconnect2,
	getDb2: getDb2,
};
