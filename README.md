# `convergedDbApp`
## Introduction
`convergedDbApp` is an example web application which showcases how a single Oracle Converged DB's can effectively store and serve multiple data-types (JSON, Spatial, and Graph). 

Traditional DB connections (JDBC, ODBC, etc.) are made through `convergedDbApp` and modern methods (RESTful) through `convergedDbAppApiService`. 

## Usage
```bash
# Start the application process
pm2 start cvg_app_index.js

# Monitor the process
pm2 monit

# Kill all processes
pm2 kill
```

## Prerequisites
### Basic Local Configuration
1. [Oracle Instance Client](https://www.oracle.com/database/technologies/instant-client/downloads.html) (19.8.0+)
2. [Oracle SQL Developer](https://www.oracle.com/tools/downloads/sqldev-downloads.html)
3. Do labs 1 - 3 of [Converged Database for Developers Workshop](https://apexapps.oracle.com/pls/apex/dbpm/r/livelabs/view-workshop?p180_id=613)
4. Start `Oracle SQL Developer`
5. Add the `OCI Workshop Computer Instance` to `SSH Host` (View > SSH)<br /> ![Edit SSH Host](assets/readme/EDIT_SSH_HOST.png)
6. Add a `New Local Port Forward` to the `SSH Host` (Right-click SSH Host > New Local Port Forward) and `Use specific local port` 1521 <br/> ![Edit Local Port Forward](assets/readme/EDIT_LOCAL_PORT_FORWARD.png)
7. Ensure both the `SSH Host` and `Local Port Forwarding` are connected
8. Clone `convergedDbApp` repository
9. Execute `npm install`
10. Edit the `hostname` in database configs (`/database/*.js`) to `127.0.0.1` and port `1521`<br /> ![Edit App Hostname](assets/readme/EDIT_HOSTNAME.png)

## Note
Both `convergedDbApp` and `convergedDbAppApiService` are standalone services

## Local DEV
### Oracle
autossh -N -M 0 -o "ServerAliveInterval 30" -o "ServerAliveCountMax 3" -L 1521:cvg-db-workshop-s01-2020-09-05-062557.pub.cvgdblab.oraclevcn.com:1521 -i ~/.ssh/oci.pem opc@129.213.41.82

### DocumentDB
autossh -N -M 0 -o "ServerAliveInterval 30" -o "ServerAliveCountMax 3" -L 27017:docdb-2020-09-08-07-52-31.cluster-c1pmmvhyc1iw.us-east-1.docdb.amazonaws.com:27017 -i ~/.ssh/AWS/aws.pem ec2-user@54.237.54.63
