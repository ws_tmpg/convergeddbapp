/**
 * Create express object.
 */
var express = require('express');
/**
 * Session object declear
 */
var session = require('express-session');
/**
 * Cookie object declear
 */
var cookieParser = require('cookie-parser');
/**
 * Create app object & assign express object.
 */

var app = express();
/**
 * Create reload object.
 */
var reload = require('reload');
/**
 * For set port or default 7000 posr.
 */
app.set('port', process.env.PORT);
/**
 * Set view engine & point a view folder.
 */
app.set('view engine', 'ejs');
app.set('views', 'views');
app.engine('html', require('ejs').renderFile);
/**
 * Register cookie
 */
app.use(cookieParser());
/**
 * Register session with secret key
 */
app.use(session({secret: 'kak'}));
/**
 * Add & register body parser
 */
var bodyParser = require('body-parser');
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({extended: true})); // support encoded bodies
/**
 * Set site title.
 */
app.locals.siteTitle = 'Ecommerce';
/**
 * set public folder is static to use any where.
 * Public folder contents js, css, images
 */
app.use(express.static('public'));
/**
 * Add routes.
 */
console.log( process.env);
console.log('SET FOR INFRASTRUCTURE: ' + process.env.INFRA_TYPE);
if(process.env.INFRA_TYPE === "ORACLE") {
    var osm = require('osm');
    const Dbconnect_docdb = require('./database/oracle/config').Dbconnect_docdb;
    const getDb_docdb = require('./database/oracle/config').getDb_docdb;
    const Dbconnect_auroradb = require('./database/oracle/config_aurora').Dbconnect_auroradb;
    const getDb_auroradb = require('./database/oracle/config_aurora').getDb_auroradb;
    app.use(require('./routers/oracle_pages'));
    Dbconnect_docdb().then(res => {
        console.log(res);
        Dbconnect_auroradb().then(res => {
            console.log(res);
            var server = app.listen(process.env.PORT, () => {
                console.log('working on port ' + process.env.PORT)
            })
        })
    }).catch(err => console.log(err))
}
else{
    app.use(require('./routers/aws_pages'));
    var server = app.listen(process.env.PORT, () => {
        console.log('working on port ' + process.env.PORT)
    })
}



/**
 * Auto reload server.
 */
// reload(server, app);
