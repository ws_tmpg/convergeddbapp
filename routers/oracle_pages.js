let express = require('express');
let session = require('express-session');
let route = express.Router();
let docdb_pdb = require('../database/oracle/config').getDb_docdb;
let auroradb_pdb = require('../database/oracle/config_aurora').getDb_auroradb;
let db3 = require('../database/oracle/analytics').getDb3;
var js2xmlparser = require("js2xmlparser");
let oracledb = require('oracledb');
oracledb.fetchAsString = [oracledb.CLOB];
const iconv = require('iconv-lite')
//oracledb.fetchAsBuffer = [oracledb.BLOB]
var convert = require('xml-js');
const X2JS = require("x2js");
//var parser = require('xml2json');
var parser = require('fast-xml-parser');
let randomStatusCounter = 0;

var osm = require('osm');
route.get('/', function (req, res) {
    res.render('home', {title: 'Home'});
});

route.get('/test', function (req, res) {
    //res.send('<h1>About page</h1>');
    console.log(js2xmlparser.parse("products", cart));
    res.render('order', {title: 'Checkout'});
});

route.get('/health-check', (req, res) => {
    return res.status(200).send({health: true});
});

route.get('/map', (req, res) => {
    const connection = auroradb_pdb();
    console.log(req.query.area)
    let areaSelected = req.query.area;
    if (areaSelected === undefined) {
        areaSelected = "MICHIGAN"
    }
    let finalArrayOfProducts = [];
    let options;
    connection.query(`select distinct area_name from store_loc`, [],
        {
            outFormat: oracledb.OBJECT
        })
        .then(result => {
            options = result.map(o => o.AREA_NAME)
            if (!options.includes(areaSelected)) {
                areaSelected = "MICHIGAN";
            }
            connection.query(`select STORE_ID,
      STORE_NAME,
      CENTER_LONG,
      CENTER_LAT,
      POSTAL_CODE from store_loc where AREA_NAME=:a`, [areaSelected],
                {
                    outFormat: oracledb.OBJECT
                }).then(result => {
                console.log(result)
                res.render('finalMaps', {options: options, title: 'Maps', data: result, selectedArea: areaSelected});
            })
        })
})


route.get('/products', (req, res) => {
    const connection = docdb_pdb();
    let finalArrayOfProducts = [];
    connection.query(`SELECT a.json_document from products a`, [],
        {
            outFormat: oracledb.OBJECT
        })
        .then(result => {
            if (result[0]) {
                res.json(JSON.parse(iconv.decode(result[0].JSON_DOCUMENT, 'utf-8')));
            } else {
                res.json(finalArrayOfProducts);
            }
        })
        .catch(err => {
            console.log(err)
        });

})

route.get('/shop', (req, res) => {
    console.log(req.query)
    const searchText = req.query.searchText;
    const connection = docdb_pdb();
    let finalArrayOfProducts = [];
    connection.query(`SELECT a.json_document from products a`, [],
        {
            outFormat: oracledb.OBJECT
        })
        .then(result => {
            result[0] && JSON.parse(iconv.decode(result[0].JSON_DOCUMENT, 'utf-8')).forEach(item => {

                    if (item.category.toLowerCase().match(searchText && searchText.toLowerCase()) || item.title.toLowerCase().match(searchText && searchText.toLowerCase())) {
                        finalArrayOfProducts.push(item)
                    }
                }
            )
            console.log(finalArrayOfProducts);
            res.render('products', {title: 'Shop', products: finalArrayOfProducts});
        })
        .catch(err => {
            console.log(err)
        });
})


route.get('/product/:product', function (req, res) {
    const connection = docdb_pdb();
    let products = [];
    connection.query(`SELECT a.json_document from products a`, [],
        {
            outFormat: oracledb.OBJECT
        })
        .then(result => {
            JSON.parse(iconv.decode(result[0].JSON_DOCUMENT, 'utf-8')).forEach(item => {

                    if (item.pid === req.params.product) {
                        products.push(item)
                    }
                }
            )
            console.log(products);
            res.render('product', {title: 'Product', products: products})
        })
        .catch(err => {
            console.log(err)
        });
});


//TODO: Add https://www.npmjs.com/package/oracledb-upsert
route.get('/add-to-cart/:product', (req, res, next) => {
    let selectedPid = req.params.product;
    console.log("SELECTED PID IS ", selectedPid)
    const connection = docdb_pdb();
    connection.query(`SELECT a.json_document from products a`, [],
        {
            outFormat: oracledb.OBJECT
        })
        .then(result => {
            let productObj;
            JSON.parse(iconv.decode(result[0].JSON_DOCUMENT, 'utf-8')).forEach(item => {

                    if (item.pid === req.params.product) {
                        productObj = {...item, quantity: 1}
                    }
                }
            )

            connection.execute(`SELECT CART_DOCUMENT FROM USER_CART WHERE ROWNUM = 1`, [],
                {}).then(result => {
                let cart;
                if (result.rows.length === 0) {
                    cart = [];
                    cart.push(productObj);
                } else {
                    cart = JSON.parse(result.rows[0][0]);
                    let foundCartItem = cart.find(cartItem => cartItem.pid == selectedPid)
                    if (foundCartItem !== undefined) {
                        foundCartItem.quantity += 1;
                    } else {
                        cart.push(productObj)
                    }
                }
                let returningCart = [...cart];
                let stringifyCart = JSON.stringify(returningCart);
                console.log("STRINGIFY CART", stringifyCart)
                connection.execute(`UPDATE user_cart SET cart_document=:stringifyCart`, [stringifyCart],
                    {
                        outFormat: oracledb.OBJECT,
                        autoCommit: true
                    }).then(result => {
                    console.log(result)
                    res.render('cart', {title: 'Cart', products: returningCart});
                })
                    .catch(err => {
                        console.log(err)
                    })
            })
                .catch(err => {
                    console.log(err)
                })
        })
});

route.get('/remove-from-cart/:pid', function (req, res) {
    let products = req.cookies.node_express_ecommerce;
    const connection = docdb_pdb();
    connection.execute(`SELECT CART_DOCUMENT FROM USER_CART WHERE ROWNUM = 1`, [],
        {}).then(result => {
        let cart;
        if (result.rows.length === 0) {
            cart = [];
        } else {
            cart = JSON.parse(result.rows[0][0]);
        }

        let deletePid = req.params.pid;
        let newCartArray = cart.filter(product => product.pid != deletePid);
        cart = [...newCartArray];
        let stringifyCart = JSON.stringify(cart);
        connection.execute(`UPDATE user_cart SET cart_document=:stringifyCart`, [stringifyCart],
            {
                outFormat: oracledb.OBJECT,
                autoCommit: true
            })
            .then(result => {
                res.redirect('/cart');
            })
    })
});

route.get('/empty-cart', function (req, res) {
    let products = [];
    const connection = docdb_pdb();
    cart = [];
    let stringifyCart = JSON.stringify(cart);
    connection.execute(`UPDATE user_cart SET cart_document=:stringifyCart`, [stringifyCart],
        {
            outFormat: oracledb.OBJECT,
            autoCommit: true
        })
        .then(result => {
            res.redirect('/cart');
        })
});

route.get('/cart', function (req, res) {
    const connection = docdb_pdb();
    connection.execute(`SELECT CART_DOCUMENT FROM USER_CART WHERE ROWNUM = 1`, [],
        {}).then(result => {
        let cart;

            if (JSON.parse(JSON.parse(JSON.stringify(result.rows[0][0])))[0]) {
                console.log(JSON.parse(JSON.parse(JSON.stringify(result.rows[0][0])))[0].cart)
                console.log("CHECK")
                //TODO: Fix this
                cart = JSON.parse(JSON.parse(result.rows[0][0])[0].cart)
            } else {
                cart = []
            }


        res.render('cart', {title: 'Cart', products: cart});
    })
});

route.post('/update-cart', function (req, res) {
    const connection = docdb_pdb();

    connection.execute(`SELECT CART_DOCUMENT FROM USER_CART WHERE ROWNUM = 1`, [],
        {}).then(result => {
        let cart;
        if (result.rows.length === 0) {
            cart = [];
        } else {
            cart = JSON.parse(result.rows[0][0]);
        }

        console.log("IN update cart", cart, req.body.qnt[0]);
        cart.forEach(function (product, index) {
            product.quantity = parseInt(req.body.qnt[index]);
        });
        console.log("after update cart", cart, req.body.qnt[0]);
        let stringifyCart = JSON.stringify(cart);
        connection.execute(`UPDATE user_cart SET cart_document=:stringifyCart`, [stringifyCart],
            {
                outFormat: oracledb.OBJECT,
                autoCommit: true
            })
            .then(result => {
                res.redirect('/cart');
            })
    })
});

route.get('/checkout', function (req, res) {
    //res.send('<h1>About page</h1>');
    res.render('order', {title: 'Checkout'});
});

route.post('/orderdetail', function (req, res) {
    //res.send('<h1>About page</h1>');
    console.log("status page", req.body)
    const {name, email, phone, address, city} = req.body;
    let orderDoc = {};
    const connection = docdb_pdb();

    connection.execute(`SELECT CART_DOCUMENT FROM USER_CART WHERE ROWNUM = 1`, [],
        {}).then(result => {
        let cart;
        if (result.rows.length === 0) {
            cart = [];
        } else {
            cart = JSON.parse(result.rows[0][0]);
        }
        orderDoc.products = cart;
        orderDoc.userDetails = req.body;
        orderDoc = js2xmlparser.parse("order", orderDoc)
        console.log(result);
        console.log("THIS IS ORDER DOC:")
        console.log(orderDoc)
        const connection2 = auroradb_pdb();
        let plsql = `DECLARE
  v_xml   SYS.XMLTYPE;
  v_doc   CLOB;
  cnt number;
BEGIN
  v_doc := :a;
  v_xml := SYS.XMLTYPE.createXML(v_doc);
  select count(*) into cnt from orders;
  if cnt=0 then
  INSERT INTO orders (order_document) VALUES (v_xml);
  else
  UPDATE orders SET order_document=v_xml;
  end if;
  END;`
        connection2.execute(plsql, [orderDoc],
            {
                outFormat: oracledb.OBJECT,
                autoCommit: true
            }).then(result => {
            cart = [];
            let stringifyCart = JSON.stringify(cart);
            connection.execute(`UPDATE user_cart SET cart_document=:stringifyCart`, [stringifyCart],
                {
                    outFormat: oracledb.OBJECT,
                    autoCommit: true
                })
            console.log(result);

        })

        res.render('order-details', {title: 'Order Status'});
    })
});

route.get('/orderstatus', (req, res) => {
    randomStatusCounter++;
    if (randomStatusCounter > 3) {
        randomStatusCounter = 3;
    }
    let status;

    if (randomStatusCounter == 1) {
        status = "Shipped"
    } else if (randomStatusCounter == 2) {
        status = "In Transit"
    } else {
        status = "Delivered";
    }
    const connection = auroradb_pdb();
    let sql = `SELECT a.order_document.getStringVal() xmldata
    FROM   orders a`;
    connection.execute(sql, [],
        {
            outFormat: oracledb.OBJECT,
            autoCommit: true
        }).then(result => {
        //console.log(result.rows[0])

        // xml to json
        // var options = {
        // object: true,
        // coerce: false,
        // sanitize: true,
        // trim: true
        // };
        var options = {
            trimValues: true,
            arrayMode: false,
        };
        console.log(result)
        var json = parser.parse(result.rows[0].XMLDATA, options);
        console.log("to json -> %s", json);
        let productsArr = [];
        console.log(json.order.products);
        if (json.order.products.length > 1) {
            productsArr = [...json.order.products];
        } else {
            productsArr.push(json.order.products)
        }
        console.log(productsArr)
        res.render('order-status', {
            status: status,
            title: 'Order Status',
            products: productsArr,
            userData: json.order.userDetails
        })
        //  res.send(json)
        // var json = parser.toJson(result.rows[0].XMLDATA,options);
        // console.log("to json -> %s", json);
        // console.log(json.order.products)
        // res.render('order-status',{status:status,title: 'Order Status',products:json.order.products,userData:json.order.userDetails})
        //  res.send(json)
    })
})

route.get('/chart', function (req, res) {
    let Total = [];
    let xlable = [];
    let data = [];
    let piechart = [];
    let price = [];
    let pricecount = [];
    let piePrice = [];
    let SALES_TYPE_NAME = [];
    let MCOUNTS = [];
    let FCOUNTS = [];
    let top5 = [];
    let SALE_DATE = [];
    let VALUE_OF_ORDERS = [];
    const connection = db();
    connection.query(`SELECT a.product_document.category, count(a.product_document.category) as counts from products a  group by a.product_document.category HAVING   Count(a.product_document.category) > 10`, [],
        {
            outFormat: oracledb.OBJECT
        })
        .then(result => {
            //console.log("working data",result);

            result.forEach(obj => {
                //console.log(obj['CATEGORY']);
                xlable.push(obj['CATEGORY']);
                data.push(obj['COUNTS']);
                piechart.push([obj['CATEGORY'], obj['COUNTS']])
            })
            const connection = db();
            connection.query(`select count(a.product_document.pid) as Total from products a`, [],
                {
                    outFormat: oracledb.OBJECT
                })
                .then(result => {
                    //console.log("working data",result);

                    result.forEach(obj => {
                        //console.log(obj['TOTAL']);
                        Total.push(obj['TOTAL']);
                    })
                    const connection = db();
                    connection.query(`SELECT a.product_document.price, count(a.product_document.price) as pricecounts from products a  group by a.product_document.price HAVING   (a.product_document.price) < 20`, [],
                        {
                            outFormat: oracledb.OBJECT
                        })
                        .then(result => {
                            //console.log("working data",result);

                            result.forEach(obj => {
                                // console.log(obj['PRICE']);
                                // console.log(obj['PRICECOUNTS']);
                                price.push(obj['PRICE'] + "$");
                                pricecount.push(obj['PRICECOUNTS']);
                                piePrice.push([obj['PRICE'] + "$", obj['PRICECOUNTS']])
                            })
                            const connection = db3();
                            connection.query(`select distinct REPLACE(st.sales_type_name,'Normal Day Sale', 'Christmas Day') as sales_type_name, count(case when gender='M' then 1 end) as Mcounts,count(case when gender='F' then 1 end) as Fcounts from  customer_order_products cop inner join sales_type st on cop.sales_type_id=st.sales_type_id group by sales_type_name`, [],
                                {
                                    outFormat: oracledb.OBJECT
                                })
                                .then(result => {
                                    console.log("working ANS", result);

                                    result.forEach(obj => {
                                        console.log(obj['SALES_TYPE_NAME']);
                                        //console.log(obj['GENDER']);
                                        //  console.log(obj['MCOUNTS']);
                                        //  console.log(obj['FCOUNTS']);
                                        SALES_TYPE_NAME.push(obj['SALES_TYPE_NAME']);
                                        MCOUNTS.push(obj['MCOUNTS']);
                                        FCOUNTS.push(obj['FCOUNTS']);

                                    })
                                    const connection = db3();
                                    connection.query(`select p.product_name, 
                count(*) number_of_orders,
                sum ( oi.quantity * oi.unit_price ) total_value,
                rank () over ( 
                  order by sum ( oi.quantity * oi.unit_price ) desc 
                ) revenue_rank
         from   products p
         join   order_items oi
         on     p.product_id = oi.product_id
         group  by p.product_name
         order  by count(*) desc
         fetch  first 5 rows only`, [],
                                        {
                                            outFormat: oracledb.OBJECT
                                        })
                                        .then(result1 => {
                                            console.log("working ANS", result1);
                                            const connection = db3();
                                            connection.query(`
                      with dates as (
                        select date'2018-02-03' + level dt 
                        from   dual
                        connect by level <= 433
                      ), order_totals as (
                        select trunc ( o.order_datetime ) order_date,
                               count ( distinct o.order_id ) number_of_orders,
                               sum ( oi.quantity * oi.unit_price ) value_of_orders
                        from   orders o
                        join   order_items oi
                        on     o.order_id = oi.order_id
                        group  by trunc ( o.order_datetime )
                      )
                        select to_char ( dt, 'DD-MON' ) sale_date, 
                               nvl ( number_of_orders, 0 ) number_of_orders, 
                               nvl ( value_of_orders, 0 ) value_of_orders
                        from   dates
                        left   join order_totals
                        on     dt = order_date
                        order  by dt  fetch first 90 rows only`, [],
                                                {
                                                    outFormat: oracledb.OBJECT
                                                })
                                                .then(result => {
                                                    //console.log("working Last 90days",result);

                                                    result.forEach(obj => {
                                                        SALE_DATE.push(obj['SALE_DATE']);
                                                        VALUE_OF_ORDERS.push(obj['VALUE_OF_ORDERS']);
                                                        //console.log(SALE_DATE);
                                                        // console.log(VALUE_OF_ORDERS);


                                                    })

                                                    //  console.log(xlable);
                                                    //  console.log(data);
                                                    //  console.log(piechart);
                                                    res.render('analytics', {
                                                        title: 'dashboard',
                                                        SALE_DATE: SALE_DATE,
                                                        VALUE_OF_ORDERS: VALUE_OF_ORDERS,
                                                        top5: result1,
                                                        SALES_TYPE_NAME: SALES_TYPE_NAME,
                                                        MCOUNTS: MCOUNTS,
                                                        FCOUNTS: FCOUNTS,
                                                        xlable: xlable,
                                                        data: data,
                                                        Total: Total,
                                                        piechart: piechart,
                                                        price: price,
                                                        pricecount: pricecount,
                                                        piePrice: piePrice
                                                    });
                                                })
                                        })
                                })
                        })
                })
        })
        .catch(err => {
            console.log(err)
        });
});

route.get('/about', function (req, res) {
    //res.send('<h1>About page</h1>');
    res.render('page', {title: 'About'});
});

route.get('/contact', function (req, res) {
    res.render('page', {title: 'Contact'});
});

module.exports = route;
