// INIT EXPRESS
let express = require('express');
let session = require('express-session');
let route = express.Router();
let url;
let dbName;
let collectionProduct;
let collectionCart;
let clientPG;
let mongoOptions;

//MONGO INIT
let MongoClient = require('mongodb').MongoClient
f = require('util').format,
    fs = require('fs');
let ca = [fs.readFileSync("./database/aws/documentDB/rds-combined-ca-bundle.pem")];
//POSTGRES INIT
const {Client, Pool} = require('pg')
var types = require('pg').types
types.setTypeParser(1700, 'text', parseFloat);



if(process.env.CONNECTION === "LOCAL") {
    mongoOptions = {
        useNewUrlParser: true,
        useUnifiedTopology: true
    };
    url = `mongodb://admin123:Admin123@${process.env.MONGO_HOSTNAME}:27017/convergedApp`;
    dbName = "convergedApp";
    collectionProduct = "products";
    collectionCart = "cart";
    clientPG  = new Pool({
        user: 'admin123',
        host: `${process.env.POSTGRES_HOSTNAME}`,
        database: 'convergedApp',
        password: 'Admin123',
        port: 5432,
    })
}
else{
    mongoOptions = {
        sslValidate: true,
        sslCA:ca,
        useNewUrlParser: true,
        useUnifiedTopology: true
    };
    url = `mongodb://admin123:Admin123@${process.env.MONGO_HOSTNAME}:27017/convergedApp?ssl=true&replicaSet=rs0&readPreference=secondaryPreferred`;
    dbName = "convergedApp";
    collectionProduct = "products";
    collectionCart = "cart";
    clientPG = new Pool({
        user: 'admin123',
        host: `${process.env.POSTGRES_HOSTNAME}`,
        database: 'convergedApp',
        password: 'Admin123',
        port: 5432,
    })
}


clientPG.connect()

//PARSER
var js2xmlparser = require("js2xmlparser");
var parser = require('fast-xml-parser');
let randomStatusCounter = 0;



//DOCUMENTDB QUERIED FOR PRODUCTS PAGE
route.get('/products', (req, res) => {
    let finalArrayOfProducts = [];


    MongoClient.connect(url,mongoOptions, (err, client) => {
        if (client) console.log("connected /products");
        if (err) console.log(err);

        let db = client.db(dbName);
        let collection = db.collection(collectionProduct);
        collection.find().toArray((err, items) => {
            if (err) console.log(err);
            res.json(items);
        })

    });
})

route.get('/shop', (req, res) => {
    console.log(req.query)
    const searchText = req.query.searchText;
    console.log(req.query.searchText);
    const searchTextQuery = {$or: [{"category": {$regex: searchText}}, {"title": {$regex: searchText}}]};
    MongoClient.connect(url, mongoOptions, (err, client) => {
        if (client) console.log("connected /shop");
        if (err) console.log(err);

        let db = client.db(dbName);
        let collection = db.collection(collectionProduct);
        if (searchText !== undefined) {
            collection.find(searchTextQuery).toArray((err, items) => {
                if (err) console.log(err);
                res.render('products', {title: 'Shop', products: items});
            })
        } else {
            collection.find().toArray((err, items) => {
                if (err) console.log(err);
                res.render('products', {title: 'Shop', products: items});
            })
        }


    })

})


route.get('/product/:product', function (req, res) {
    let products = [];

    MongoClient.connect(url, mongoOptions, (err, client) => {
        if (client) console.log("connected /product/:product");
        if (err) console.log(err);

        let db = client.db(dbName);
        let collection = db.collection(collectionProduct);
        collection.find({pid: {$in: [req.params.product]}}).toArray((err, items) => {
            if (err) console.log(err);
            res.render('product', {title: 'Product', products: items})
        })
    });
});


route.get('/', function (req, res) {
    res.render('home', {title: 'Home'});
});


route.get('/add-to-cart/:product', (req, res, next) => {
    let selectedPid = req.params.product;
    console.log("SELECTED PID IS ", selectedPid)


    MongoClient.connect(url, mongoOptions, (err, client) => {
        if (client) console.log("connected /add-to-cart");
        if (err) console.log(err);

        let db = client.db(dbName);
        let collection = db.collection(collectionProduct);
        collection.findOne({pid: selectedPid}, (err, item) => {
            if (err) console.log(err);
            let productObj = {
                ...item,
                quantity: 1,
                _id: "048thj09324jg0384ng935n4g90wjefiunwdifn0923jnf92unb4g093hr9gunr"
            }
            console.log(productObj)

            let collectionUserCart = db.collection(collectionCart);

            collectionUserCart.findOne({"cart": {$exists: true, $ne: null}}, (err, item) => {
                if (err) console.log(err);
                console.log(item);
                let cart;
                if (item === null) {
                    cart = [];
                    cart.push(productObj);
                } else {
                    cart = JSON.parse(item.cart);

                    let foundCartItem = cart.find(cartItem => cartItem.pid === selectedPid)
                    if (foundCartItem !== undefined) {
                        foundCartItem.quantity += 1;
                    } else {
                        cart.push(productObj)
                    }
                }
                console.log(cart)
                let returningCart = [...cart];
                let stringifyCart = JSON.stringify(returningCart);
                console.log("STRINGIFY CART", stringifyCart)
                collectionUserCart.updateOne({}, {$set: {cart: stringifyCart}}, {upsert: true}).then((err) => {
                        if (err) console.log(err);

                        console.log(req);
                        console.log(req._parsedUrl.pathname);
                        res.render('cart', {title: 'Cart', products: returningCart});

                    }
                )
            });
        });


    });
});


route.get('/remove-from-cart/:pid', function (req, res) {

    MongoClient.connect(url, mongoOptions, (err, client) => {
        if (client) console.log("connected /remove-from-cart");
        if (err) console.log(err);
        let db = client.db(dbName);


        let collectionUserCart = db.collection(collectionCart);
        collectionUserCart.findOne({"cart": {$exists: true, $ne: null}}, (err, item) => {
            if (err) console.log(err);
            console.log(item);
            let cart;
            if (item === null) {
                cart = [];
            } else {
                cart = JSON.parse(item.cart);
            }

            let deletePid = req.params.pid;
            let newCartArray = cart.filter(product => product.pid !== deletePid);
            console.log(newCartArray)
            cart = [...newCartArray];
            let stringifyCart = JSON.stringify(cart);
            let collectionUserCart = db.collection(collectionCart);
            collectionUserCart.updateOne({}, {$set: {cart: stringifyCart}}, {upsert: true}).then((err) => {
                    if (err) console.log(err);
                    res.redirect('/cart');
                }
            )
        });
    });
});

route.get('/empty-cart', function (req, res) {

    MongoClient.connect(url, mongoOptions, (err, client) => {
        if (client) console.log("connected /empty-cart");
        if (err) console.log(err);
        let db = client.db(dbName);
        let cart = [];
        let stringifyCart = JSON.stringify(cart);
        let collectionUserCart = db.collection(collectionCart);
        collectionUserCart.updateOne({}, {$set: {cart: stringifyCart}}, {upsert: true}).then((err) => {
                if (err) console.log(err);
                res.redirect('/cart');
            }
        )
    });
});


route.post('/update-cart', function (req, res) {

    MongoClient.connect(url, mongoOptions, (err, client) => {
        if (client) console.log("connected /update-cart");
        if (err) console.log(err);
        let db = client.db(dbName);
        let collectionUserCart = db.collection(collectionCart);
        collectionUserCart.findOne({"cart": {$exists: true, $ne: null}}, (err, item) => {
            if (err) console.log(err);
            console.log(item);
            let cart;
            if (item === null) {
                cart = [];
            } else {
                cart = JSON.parse(item.cart);
            }
            console.log("IN update cart", cart, req.body.qnt[0]);
            cart.forEach(function (product, index) {
                product.quantity = parseInt(req.body.qnt[index]);
            });
            console.log("after update cart", cart, req.body.qnt[0]);

            let stringifyCart = JSON.stringify(cart);
            let collectionUserCart = db.collection(collectionCart);
            collectionUserCart.updateOne({}, {$set: {cart: stringifyCart}}, {upsert: true}).then((err) => {
                    if (err) console.log(err);
                    res.redirect('/cart');
                }
            )
        });
    });
});


route.get('/cart', function (req, res) {

    MongoClient.connect(url, mongoOptions, (err, client) => {
        if (client) console.log("connected /cart");
        if (err) console.log(err);
        let db = client.db(dbName);
        let collectionUserCart = db.collection(collectionCart);
        collectionUserCart.findOne({"cart": {$exists: true, $ne: null}}, (err, item) => {
            if (err) console.log(err);
            let cart;
            if (item === null) {
                cart = [];
            } else {
                cart = JSON.parse(item.cart);
            }

            res.render('cart', {title: 'Cart', products: cart});
        });
    });
});
route.get('/checkout', function (req, res) {
    //res.send('<h1>About page</h1>');
    res.render('order', {title: 'Checkout'});
});


route.get('/about', function (req, res) {
    //res.send('<h1>About page</h1>');
    res.render('page', {title: 'About'});
});

route.get('/contact', function (req, res) {
    res.render('page', {title: 'Contact'});
});


route.get('/map', (req, res) => {
    let areaSelected = req.query.area;
    if (areaSelected === undefined) {
        areaSelected = "MICHIGAN"
    }
    let options;

    clientPG.query('select DISTINCT AREA_NAME from PUBLIC.STORE_LOC', [], (err, res1) => {
        options = res1.rows.map(o => o.area_name)
        if (!options.includes(areaSelected)) {
            areaSelected = 'MICHIGAN';
        }
        clientPG.query(`select STORE_ID,
      STORE_NAME,
      CENTER_LONG,
      CENTER_LAT,
      POSTAL_CODE from PUBLIC.STORE_LOC where AREA_NAME='${areaSelected}'`, [], (err, res2) => {
            res.render('finalMaps', {
                options: options,
                title: 'Maps',
                data: JSON.parse(JSON.stringify(res2.rows).toUpperCase()),
                selectedArea: areaSelected
            });

        })
    })
})




route.post('/orderdetail', function (req, res) {
    MongoClient.connect(url, mongoOptions, (err, client) => {
        if (client) console.log("connected /orderdetail");
        if (err) console.log(err);
        let db = client.db(dbName);
        let collectionUserCart = db.collection(collectionCart);
        collectionUserCart.findOne({"cart": {$exists: true, $ne: null}}, (err, item) => {
            if (err) console.log(err);
            let cart;
            if (item === null) {
                cart = [];
            } else {
                cart = JSON.parse(item.cart);
            }

            const {name, email, phone, address, city} = req.body;
            let orderDoc = {};
            orderDoc.products = cart;
            orderDoc.userDetails = req.body;
            orderDoc = js2xmlparser.parse("order", orderDoc)

            clientPG.query(`SELECT * FROM PUBLIC.ORDERS`, [], (err, res1) => {
                console.log(res1.rows[0])
                if (!res1.rows[0]) {
                    clientPG.query(`INSERT INTO orders(order_document) VALUES ($1)`, [orderDoc], (err, res2) => {
                        if (err) console.log(err);
                        console.log(res2)

                    })
                } else {
                    clientPG.query(`UPDATE orders SET order_document=$1`, [orderDoc], (err, res2) => {
                        if (err) console.log(err);
                        console.log(res2)

                    })
                }
            })
            res.render('order-details', {title: 'Order Status'});

        });
    });
});

route.get('/orderstatus', (req, res) => {
    randomStatusCounter++;
    if (randomStatusCounter > 3) {
        randomStatusCounter = 3;
    }
    let status;

    if (randomStatusCounter == 1) {
        status = "Shipped"
    } else if (randomStatusCounter == 2) {
        status = "In Transit"
    } else {
        status = "Delivered";
    }
        clientPG.query(`SELECT order_document FROM PUBLIC.ORDERS`, [], (err, result) => {
            console.log("HELLO")
            console.log(result.rows[0])

        var options = {
            trimValues: true,
            arrayMode: false,
        };
        var json = parser.parse(result.rows[0].order_document, options);
        console.log("to json -> %s", json);
        let productsArr = [];
        if (json.order.products.length > 1) {
            productsArr = [...json.order.products];
        } else {
            productsArr.push(json.order.products)
        }
        console.log(productsArr)
        res.render('order-status', {
            status: status,
            title: 'Order Status',
            products: productsArr,
            userData: json.order.userDetails
        })
    })
})

/*
let db = require('../database/oracle/config').getDb;
let db2 = require('../database/oracle/configSpatial').getDb2;
let db3 = require('../database/oracle/analytics').getDb3;
var js2xmlparser = require("js2xmlparser");
let oracledb = require('oracledb');
var convert = require('xml-js');
const X2JS = require("x2js");
//var parser = require('xml2json');
var parser = require('fast-xml-parser');



var osm = require('osm');
route.get('/chart', function (req, res) {
    let Total = [];
    let xlable = [];
    let data = [];
    let piechart = [];
    let price = [];
    let pricecount = [];
    let piePrice = [];
    let SALES_TYPE_NAME = [];
    let MCOUNTS = [];
    let FCOUNTS = [];
    let top5 = [];
    let SALE_DATE = [];
    let VALUE_OF_ORDERS = [];
    const connection = db();
    connection.query(`SELECT a.product_document.category, count(a.product_document.category) as counts from products a  group by a.product_document.category HAVING   Count(a.product_document.category) > 10`, [],
        {
            outFormat: oracledb.OBJECT
        })
        .then(result => {
            //console.log("working data",result);

            result.forEach(obj => {
                //console.log(obj['CATEGORY']);
                xlable.push(obj['CATEGORY']);
                data.push(obj['COUNTS']);
                piechart.push([obj['CATEGORY'], obj['COUNTS']])
            })
            const connection = db();
            connection.query(`select count(a.product_document.pid) as Total from products a`, [],
                {
                    outFormat: oracledb.OBJECT
                })
                .then(result => {
                    //console.log("working data",result);

                    result.forEach(obj => {
                        //console.log(obj['TOTAL']);
                        Total.push(obj['TOTAL']);
                    })
                    const connection = db();
                    connection.query(`SELECT a.product_document.price, count(a.product_document.price) as pricecounts from products a  group by a.product_document.price HAVING   (a.product_document.price) < 20`, [],
                        {
                            outFormat: oracledb.OBJECT
                        })
                        .then(result => {
                            //console.log("working data",result);

                            result.forEach(obj => {
                                // console.log(obj['PRICE']);
                                // console.log(obj['PRICECOUNTS']);
                                price.push(obj['PRICE'] + "$");
                                pricecount.push(obj['PRICECOUNTS']);
                                piePrice.push([obj['PRICE'] + "$", obj['PRICECOUNTS']])
                            })
                            const connection = db3();
                            connection.query(`select distinct REPLACE(st.sales_type_name,'Normal Day Sale', 'Christmas Day') as sales_type_name, count(case when gender='M' then 1 end) as Mcounts,count(case when gender='F' then 1 end) as Fcounts from  customer_order_products cop inner join sales_type st on cop.sales_type_id=st.sales_type_id group by sales_type_name`, [],
                                {
                                    outFormat: oracledb.OBJECT
                                })
                                .then(result => {
                                    console.log("working ANS", result);

                                    result.forEach(obj => {
                                        console.log(obj['SALES_TYPE_NAME']);
                                        //console.log(obj['GENDER']);
                                        //  console.log(obj['MCOUNTS']);
                                        //  console.log(obj['FCOUNTS']);
                                        SALES_TYPE_NAME.push(obj['SALES_TYPE_NAME']);
                                        MCOUNTS.push(obj['MCOUNTS']);
                                        FCOUNTS.push(obj['FCOUNTS']);

                                    })
                                    const connection = db3();
                                    connection.query(`select p.product_name, 
                count(*) number_of_orders,
                sum ( oi.quantity * oi.unit_price ) total_value,
                rank () over ( 
                  order by sum ( oi.quantity * oi.unit_price ) desc 
                ) revenue_rank
         from   products p
         join   order_items oi
         on     p.product_id = oi.product_id
         group  by p.product_name
         order  by count(*) desc
         fetch  first 5 rows only`, [],
                                        {
                                            outFormat: oracledb.OBJECT
                                        })
                                        .then(result1 => {
                                            console.log("working ANS", result1);
                                            const connection = db3();
                                            connection.query(`
                      with dates as (
                        select date'2018-02-03' + level dt 
                        from   dual
                        connect by level <= 433
                      ), order_totals as (
                        select trunc ( o.order_datetime ) order_date,
                               count ( distinct o.order_id ) number_of_orders,
                               sum ( oi.quantity * oi.unit_price ) value_of_orders
                        from   orders o
                        join   order_items oi
                        on     o.order_id = oi.order_id
                        group  by trunc ( o.order_datetime )
                      )
                        select to_char ( dt, 'DD-MON' ) sale_date, 
                               nvl ( number_of_orders, 0 ) number_of_orders, 
                               nvl ( value_of_orders, 0 ) value_of_orders
                        from   dates
                        left   join order_totals
                        on     dt = order_date
                        order  by dt  fetch first 90 rows only`, [],
                                                {
                                                    outFormat: oracledb.OBJECT
                                                })
                                                .then(result => {
                                                    //console.log("working Last 90days",result);

                                                    result.forEach(obj => {
                                                        SALE_DATE.push(obj['SALE_DATE']);
                                                        VALUE_OF_ORDERS.push(obj['VALUE_OF_ORDERS']);
                                                        //console.log(SALE_DATE);
                                                        // console.log(VALUE_OF_ORDERS);


                                                    })

                                                    //  console.log(xlable);
                                                    //  console.log(data);
                                                    //  console.log(piechart);
                                                    res.render('analytics', {
                                                        title: 'dashboard',
                                                        SALE_DATE: SALE_DATE,
                                                        VALUE_OF_ORDERS: VALUE_OF_ORDERS,
                                                        top5: result1,
                                                        SALES_TYPE_NAME: SALES_TYPE_NAME,
                                                        MCOUNTS: MCOUNTS,
                                                        FCOUNTS: FCOUNTS,
                                                        xlable: xlable,
                                                        data: data,
                                                        Total: Total,
                                                        piechart: piechart,
                                                        price: price,
                                                        pricecount: pricecount,
                                                        piePrice: piePrice
                                                    });
                                                })
                                        })
                                })
                        })
                })
        })
        .catch(err => {
            console.log(err)
        });
});
*/

module.exports = route;
